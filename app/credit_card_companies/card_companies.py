from dataclasses import dataclass

@dataclass
class Visa:
    fullName: str
    number: str
    expiration: str
    cvv: str
    totalAmount: str
@dataclass
class MasterCard:
    first_name: str
    last_name: str
    card_number: str
    expiration: str
    cvv: str
    charge_amount: str
