from marshmallow import fields, Schema


class ChargeBodySchema(Schema):
    fullName = fields.String(required=True)
    creditCardNumber = fields.String(required=True)
    creditCardCompany = fields.String(required=True)
    expirationDate = fields.String(required=True)
    cvv = fields.String(required=True)
    amount = fields.Decimal(required=True)
