from flask import Flask
from flask_restful import Api


def create_app():
    app = Flask(__name__)
    api = Api(app)

    from .resources import main_api, charges_api,charges_statuses
    api.add_resource(main_api, '/healthcheck')
    api.add_resource(charges_api, '/api/charge')
    api.add_resource(charges_statuses, '/api/chargeStatuses')
    return app


if __name__ == '__main__':
    application = create_app()
