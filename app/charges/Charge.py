import time
from flask import make_response

from app.consts.request import MAX_RETRIES
from app.consts.response import STATUS_CODE, DECLINE_REASON, INSUFFICIENT_FUNDS, ERROR


class Charge:
    """Class represent single charge, handle recharge and response from charge"""
    def __init__(self, charge_callback):
        self.retries = 1
        self.charge_callback = charge_callback

    def charge_card(self):
        """
        :return: appropriate response according to response from charge callback.
        """
        return self.handle_response(self.charge_callback())

    def handle_response(self, response):
        """
        :param response: response from charge service.
        :return: appropriate response with status code and error if needed.
        """
        if response[STATUS_CODE] == 200:
            return make_response({}, 200)
        else:
            if response[DECLINE_REASON] != INSUFFICIENT_FUNDS and self.retries <= MAX_RETRIES:
                return self.retry_to_charge()
            else:
                return make_response({ERROR: response[DECLINE_REASON]}, 200)

    def retry_to_charge(self):
        """
        In case of failure charge, retry charging, waiting time between charges: (attempt number)^2, max retries = self.max_retries.
        :return:appropriate response according to response from charge callback.
        """
        print(f"try again in {self.retries**2}")
        time.sleep(self.retries**2)
        self.retries += 1
        return self.handle_response(self.charge_callback())