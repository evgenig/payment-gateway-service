import json
import requests
from app.charges.Charge import Charge
from app.consts.cards import VISA, MASTERCARD
from app.consts.charge_companies_endpoints import VISA_ENDPOINT, MASTERCARD_ENDPOINT
from app.consts.charge_details import CREDIT_CARD_COMPANY, FULL_NAME, CREDIT_CARD_NUMBER, EXPIRATION_DATE, CVV, AMOUNT
from app.consts.request import MERCHANT_IDENTIFIER
from app.consts.response import STATUS_CODE, CHARGE_RESULT, FAILURE, RESULT_REASON, DECLINE_REASON, REASON, COUNT
from app.credit_card_companies.card_companies import Visa, MasterCard
from functools import partial


class ChargesManager:
    """A class for managing all charges, call all the different charge services as needed."""
    def __init__(self):
        self.company_to_parser = {
            VISA: self.charge_visa,
            MASTERCARD: self.charge_master_card
        }
        self.all_charges = {}

    def charge_by_credit_card(self, charge_details, charge_headers):
        """
        charge any card from self.company_to_parser.
        :param charge_details: all required fields for making a charge.
        :param charge_headers: headers includes merchant-identifier.
        :return: response with appropriate status code, in case of error respond with appropriate error.
        """
        identifier = charge_headers[MERCHANT_IDENTIFIER]
        company = charge_details[CREDIT_CARD_COMPANY]
        charge_card_request_callback = partial(self.company_to_parser[company], charge_details, identifier)
        charger = Charge(charge_card_request_callback)
        return charger.charge_card()

    def charge_visa(self, charge_details, identifier):
        """
        create the right payload from charge_details for charging visa card.
        :param charge_details: all details required for making charge of visa card.
        :param identifier: the owner of the visa card.
        :return: generate appropriate payload for charging visa card.
        """
        body = Visa(charge_details[FULL_NAME], charge_details[CREDIT_CARD_NUMBER], charge_details[EXPIRATION_DATE], charge_details[CVV], str(charge_details[AMOUNT])).__dict__
        response = self.make_charge(VISA_ENDPOINT, body, identifier).json()
        generic_response_object = {STATUS_CODE: 200}
        if response[CHARGE_RESULT] == FAILURE:
            decline_reason = response[RESULT_REASON]
            generic_response_object[STATUS_CODE] = 400
            generic_response_object[DECLINE_REASON] = decline_reason

            self.increment_count_of_declined_charges(identifier, decline_reason)
        return generic_response_object

    def charge_master_card(self, charge_details, identifier):
        """
        :param charge_details: all details required for making charge of master card.
        :param identifier: the owner of the master card.
        :return: generate appropriate payload for charging master card.
        """
        expiration_from_request = charge_details[EXPIRATION_DATE].split("/")
        expiration_in_right_format = '-'.join([expiration_from_request[0], expiration_from_request[1]])
        splitted_full_name = charge_details[FULL_NAME].split(" ")
        first_name = splitted_full_name[0]
        last_name = " "
        if len(splitted_full_name) > 1:
            last_name = splitted_full_name[1]
        body = MasterCard(first_name, last_name, charge_details[CREDIT_CARD_NUMBER], expiration_in_right_format, charge_details[CVV], str(charge_details[AMOUNT])).__dict__
        response = self.make_charge(MASTERCARD_ENDPOINT, body, identifier)
        generic_response_object = {STATUS_CODE: 200}
        if response.status_code == 400:
            decline_reason = response.json()[DECLINE_REASON]
            generic_response_object[STATUS_CODE] = 400
            generic_response_object[DECLINE_REASON] = decline_reason
            self.increment_count_of_declined_charges(identifier, decline_reason)
        return generic_response_object

    @staticmethod
    def make_charge(endpoint, body, merchant_id):
        """
        :param endpoint: url to the specific charge service.
        :param body: appropriate body for the specific charge service.
        :param merchant_id: name of card's owner.
        :return:
        """
        headers = {
            'content-type': 'application/json',
            "identifier": f'{merchant_id}',
        }
        return requests.post(endpoint, data=json.dumps(body), headers=headers)

    def increment_count_of_declined_charges(self, merchant_id, fail_reason):
        """
        :param merchant_id: name of card's owner .
        :param fail_reason: reason of charge failure.
        :return:
        """
        is_reason_exists = False
        if merchant_id in self.all_charges.keys():
            for reason in self.all_charges[merchant_id]:
                if reason[REASON] == fail_reason:
                    reason[COUNT] += 1
                    is_reason_exists = True
                    break
            if not is_reason_exists:
                self.all_charges[merchant_id].append({REASON: fail_reason, COUNT: 1})

        else:
            self.all_charges[merchant_id] = [{REASON: fail_reason, COUNT: 1}]

