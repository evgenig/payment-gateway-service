from flask_restful import Resource
from flask import request,jsonify,make_response
import requests

from app.charges.charges_manager import ChargesManager
from app.schemas.charge_fields import ChargeBodySchema
from app.settings import MOCK_SERVER_URL

from marshmallow import ValidationError
charger = ChargesManager()

class Main(Resource):

    def get(self):
        mock_server_response = requests.get("{}/healthcheck".format(MOCK_SERVER_URL))
        print("Mock server responded to health check with the following response: {}".format(mock_server_response.text))

        return {
            "status": 'OK'
        }

class Charge(Resource):
    def post(self):
        charge_request_data = request.json
        charge_headers = request.headers
        schema = ChargeBodySchema()
        try:
            charge_details = schema.load(charge_request_data)
            response = charger.charge_by_credit_card(charge_details, charge_headers)
        except (ValidationError, KeyError) as err:
            return make_response({}, 400)
        except Exception:
            return make_response({}, 500)
        return response

class ChargeStatuses(Resource):
    def get(self):
        try:
            return make_response(jsonify(charger.all_charges[request.headers["merchant-identifier"]]), 200)
        except KeyError:
            try:
                return make_response(jsonify({"error": f"{request.headers['merchant-identifier']} does not exists"}), 401)
            except KeyError:
                return make_response({}, 400)
